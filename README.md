# Notes

## higher Order Components

You may hear terms such as ```dumb``` or ```skinny``` and ```smart or ```fat components```. These are synonyms for presentational and container components, with the latter being more recent additions to React terminology.